import module.Date;

public class App {
    public static void main(String[] args) throws Exception {
        Date date1 = new Date(24, 03, 2023);

        Date date2 = new Date(25, 03, 2023);
        System.out.println("Date 1: " + date1.toString());
        System.out.println("Date 2: " + date2.toString());
    }
}
